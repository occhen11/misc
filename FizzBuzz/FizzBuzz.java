/**
@author Olivia Chen
FizzBuzz class for the fizzbuzz problem.
 */

/** For consecutive numbers 1 to ARGS[0], or 100 if no argument is input, inclusive,
 * prints "Fizz" if a number is divisible by 3, "Buzz" if a number is divisible by 5
 * "FizzBuzz" if a number is divisible by both 3 and 5, and the number itself if
 * none apply. If more than one argument is input, a usage message is first printed 
 * and the method proceeds, but only the first argument is considered.
 */
public class FizzBuzz {
    public static void main(String[] args) {
	int n;
	if (args.length > 0) {
	    n = Integer.parseInt(args[0]);
	    if (args.length > 1) {
		System.out.println("Usage : java FizzBuzz [ N ]\n\t"
				   +"N: the upper limit of the FizzBuzz problem;"
				   +" optional. Default is 100.\n\t"
				   +"Additional arguments are ignored.\n\n");
	    }
	} else {
	    n = 100;
	}
	for (int i = 1; i <= n; i+= 1) {
	    if (i % 15 == 0) {
		System.out.println("FizzBuzz");
	    } else if (i % 3 == 0) {
		System.out.println("Fizz");
	    } else if (i % 5 == 0) {
		System.out.println("Buzz");
	    } else {
		System.out.println(i);
	    }
	}
    }
}
